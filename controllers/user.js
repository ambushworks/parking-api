const User = require('../models/user');

// for staff to create a customer for a car, without password.
exports.create = async (req, res, next) => {
  try {
    const user = new User({
      email: req.body.email,
      phone: req.body.phone,
      role: 'user',
    });

    try {
      const result = await user.save();
      res.status(201).json({ id: result._id });
    } catch (err) {
      if (err.code === 11000) {
        if (!err.statusCode) {
          const error = new Error('Phone or email already exists.');
          error.statusCode = 303;
          next(err);
        }
      }
    }
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.update = async (req, res, next) => {
  const { id } = req.params;
  const user = await User.findById(id);

  try {
    if (!user) {
      const error = new Error('User not found.');
      error.statusCode = 404;
      throw error;
    }

    user.phone = req.body.phone;
    user.email = req.body.email;

    const result = await user.save();
    res.status(200).json({ id: result._id });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};
