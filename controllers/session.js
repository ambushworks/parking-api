const Session = require('../models/session');
const Rate = require('../models/rate');

exports.getClosedSessions = async (req, res, next) => {
  try {
    const { days } = req.params;
    const result = await Session.getClosed(days);
    res.status(200).json(result);
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.getOpenSessions = async (req, res, next) => {
  try {
    const result = await Session.getOpen();
    res.status(200).json(result);
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.createSession = async (req, res, next) => {
  try {
    const session = new Session({
      car: req.body.car,
      startedBy: req.userId,
      started: Date(),
      rate: '65aed4fabd5f05561be9c31a',
    });
    const result = await session.save();
    res.status(201).json({ id: result._id, started: result.started });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.updateSession = async (req, res, next) => {
  const { id } = req.params;
  const session = await Session.findById(id).populate('rate').populate('car');

  try {
    if (!session) {
      const error = new Error('Session not found.');
      error.statusCode = 404;
      throw error;
    }

    res.status(200).json({});
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.endSession = async (req, res, next) => {
  const { id } = req.params;
  const session = await Session.findById(id)
    .populate('rate')
    .populate({
      path: 'car',
      populate: {
        path: 'customer',
        model: 'User',
      },
    })
    .populate('startedBy');

  try {
    if (!session || id == null) {
      const error = new Error('Session not found.');
      error.statusCode = 404;
      throw error;
    }

    if (typeof session.ended !== 'undefined' && session.ended !== null) {
      const error = new Error('Session has already ended.');
      error.statusCode = 403;
      throw error;
    }

    session.end();
    const result = await session.save();

    const { car } = session;
    car.lastSession = session.ended;
    car.save();

    res.status(200).json(result);
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.addRate = async (req, res, next) => {
  try {
    const rate = new Rate({
      name: req.body.name,
      periods: req.body.periods,
      default: req.body.default,
    });
    const result = await rate.save();

    // ensure previous default is set to false.
    if (result.default === true) {
      result.setDefault();
    }
    res.status(201).json({ message: 'Rate created.', rateId: result._id });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};
