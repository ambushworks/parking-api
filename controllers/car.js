const Car = require('../models/car');

exports.addCar = async (req, res, next) => {
  try {
    const plate = req.body.plate.toUpperCase();
    const car = new Car({
      plate,
    });
    const result = await car.save();
    res.status(201).json({ id: result._id, plate: result.plate });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.getCars = async (req, res, next) => {
  Car.withoutOpenSessions()
    .then((cars) => {
      res.status(200).json(cars);
      next();
    }).catch((err) => {
      console.log(err);
      const error = new Error('No cars found.');
      error.statusCode = 404;
      next(err);
    });
};

exports.updateCar = async (req, res, next) => {
  const { id } = req.params;
  try {
    // Assuming you want to return the updated document and run validators
    const updatedCar = await Car.findByIdAndUpdate(
      id,
      req.body,
      { new: true, runValidators: true },
    ).populate('customer');
    if (!updatedCar) {
      return res.status(404).send({ message: 'Car not found' });
    }
    res.status(200).json(updatedCar);
  } catch (err) {
    next(err);
  }
  return next();
};

exports.searchByPlate = async (req, res, next) => {
  const { plate } = req.params;
  const result = await Car.searchByPlate(plate);

  if (result.length > 0) {
    res.status(200).json(result);
    next();
  } else {
    const error = new Error('No cars found.');
    error.statusCode = 404;
    next(error);
  }
};
