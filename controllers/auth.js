const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const TokenGenerator = require('../utils/token-generator');

const config = require('../config/config');

const User = require('../models/user');

exports.signup = async (req, res, next) => {
  try {
    const { email } = req.body;
    const { password } = req.body;

    const hashedPw = await bcrypt.hash(password, 12);
    const user = new User({
      email,
      password: hashedPw,
    });
    const result = await user.save();
    res.status(201).json({ message: 'User created', userId: result._id });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.login = async (req, res, next) => {
  const { email } = req.body;
  const { password } = req.body;
  let loadedUser;

  try {
    const user = await User.findOne({ email }, '+password');
    if (!user) {
      const error = new Error('User was not found.');
      error.statusCode = 404;
      throw error;
    }
    loadedUser = user;

    const isEqual = await bcrypt.compare(password, user.password);
    if (!isEqual) {
      const error = new Error('Incorrrect password.');
      error.statusCode = 401;
      throw error;
    }

    const tokenGenerator = new TokenGenerator({ noTimestamp: false });
    const newTokens = tokenGenerator.generate(loadedUser._id.toString());
    res.status(200).json(newTokens);
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.refresh = (req, res, next) => {
  const { token } = req.body;

  let payload;
  try {
    payload = jwt.verify(token, config.refreshTokenSecret);

    const tokenGenerator = new TokenGenerator({ noTimestamp: false });
    const newTokens = tokenGenerator.generate(payload.userId);
    res.status(200).json(newTokens);
  } catch (err) {
    if (err instanceof jwt.TokenExpiredError) {
      err.statusCode = 401;
    } else {
      err.statusCode = 500;
    }
    next(err);
  }
};
