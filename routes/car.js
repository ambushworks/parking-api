const express = require('express');
const { body } = require('express-validator');

const carController = require('../controllers/car');

const isAuth = require('../middleware/is-auth');
const isStaff = require('../middleware/is-staff');
const isValid = require('../middleware/is-valid');

const router = express.Router();

router.post(
  '/',
  isAuth,
  [
    body('plate')
      .trim()
      .not().isEmpty()
      .matches(/^[0-9A-Za-z-]+$/)
      .withMessage('Licence plate contains forbidden characters.'),
  ],
  isValid,
  carController.addCar,
);

router.patch(
  '/:id',
  isAuth,
  isStaff,
  carController.updateCar,
);

router.get(
  '/',
  isAuth,
  isStaff,
  carController.getCars,
);

router.get(
  '/search/:plate',
  isAuth,
  isStaff,
  carController.searchByPlate,
);

module.exports = router;
