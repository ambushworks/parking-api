const express = require('express');
const { body } = require('express-validator');

const User = require('../models/user');
const authController = require('../controllers/auth');

const isValid = require('../middleware/is-valid');

const router = express.Router();

router.put(
  '/signup',
  [
    body('email')
      .isEmail()
      .withMessage('Please enter a valid email.')
      .custom((value) => User.findOne({ email: value })
        .then((userDoc) => {
          if (userDoc) {
            throw new Error('E-Mail already exists.');
          }
        }))
      .normalizeEmail(),
    body('password').trim().isLength({ min: 5 }),
  ],
  isValid,
  authController.signup,
);

router.post('/login', authController.login);

router.post('/refresh', authController.refresh);

module.exports = router;
