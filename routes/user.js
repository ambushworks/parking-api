const express = require('express');
// const { body } = require('express-validator');

// const User = require('../models/user');
const userController = require('../controllers/user');

const isAuth = require('../middleware/is-auth');
const isStaff = require('../middleware/is-staff');

const router = express.Router();

router.post(
  '/',
  isAuth,
  isStaff,
  [
    // body('email'),
  ],
  userController.create,
);

router.patch(
  '/:id',
  isAuth,
  isStaff,
  userController.update,
);

module.exports = router;
