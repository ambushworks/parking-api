const express = require('express');
const { body } = require('express-validator');

const Session = require('../models/session');
const sessionController = require('../controllers/session');

const isAuth = require('../middleware/is-auth');
const isValid = require('../middleware/is-valid');
const isAdmin = require('../middleware/is-admin');
const isStaff = require('../middleware/is-staff');

const router = express.Router();

router.get(
  '/open',
  isAuth,
  isStaff,
  sessionController.getOpenSessions,
);

router.get(
  '/closed/:days',
  isAuth,
  isStaff,
  sessionController.getClosedSessions,
);

router.post(
  '/',
  isAuth,
  isStaff,
  [
    body('carId')
      .custom(
        async (value) => {
          const sessionDoc = await Session.findOne(
            {
              car: value,
              $or: [{ out: null }, { out: { $exists: false } }],
            },
          );
          if (sessionDoc) {
            throw new Error('Car already has an open session.');
          }
        },
      ),
  ],
  isValid,
  sessionController.createSession,
);

router.patch(
  '/:id',
  isAuth,
  isStaff,
  sessionController.updateSession,
);

router.patch(
  '/end/:id',
  isAuth,
  isStaff,
  sessionController.endSession,
);

router.post(
  '/add-rate',
  isAuth,
  isAdmin,
  [
    body('periods.*.hours').isNumeric(),
    body('periods.*.rate').isNumeric(),
  ],
  sessionController.addRate,
);

module.exports = router;
