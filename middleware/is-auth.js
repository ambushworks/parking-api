const User = require('../models/user');

module.exports = (req, res, next) => {
  try {
    const decodedToken = User.authorize(req);
    req.userId = decodedToken.userId;
    next();
  } catch (err) {
    next(err);
  }
};
