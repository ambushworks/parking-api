const User = require('../models/user');

/**
 * Checks that the user has the admin role.
 *
 * Expects to have access to req.userId that the isAuth middleware
 * places in the request, so must be run after isAuth.
 *
 */
module.exports = async (req, res, next) => {
  if (req.userId) {
    const user = await User.findById(req.userId);
    if (typeof user.role !== 'undefined' && user.role === 'admin') {
      next();
    } else {
      const error = new Error('Need administrator privileges.');
      error.statusCode = 401;
      next(error);
    }
  } else {
    const error = new Error('User not authenticated.');
    error.statusCode = 401;
    next(error);
  }
};
