const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const authRoutes = require('./routes/auth');
const userRoutes = require('./routes/user');
const carRoutes = require('./routes/car');
const sessionRoutes = require('./routes/session');

const config = require('./config/config');

const MONGO_DATABASE_URI = `mongodb+srv://${config.dbUser}:${config.dbPassword}@${config.deployment}/${config.db}?${config.dbParams}`;

const app = express();

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, Accept');
  next();
});

app.use('/auth', authRoutes);
app.use('/users', userRoutes);
app.use('/park/cars', carRoutes);
app.use('/park/sessions', sessionRoutes);

app.use((error, req, res, next) => {
  const status = error.statusCode || 500;
  const { message } = error;
  const { data } = error;
  res.status(status).json({ message, data });
  next(error);
});

mongoose.connect(MONGO_DATABASE_URI).then(() => {
  app.listen(config.serverPort);
}).catch((err) => console.log(err));
