const mongoose = require('mongoose');

const { Schema } = mongoose;

const BaseSchema = require('./base');

const sessionSchema = new Schema(
  {
    started: {
      type: Date,
      required: true,
    },
    ended: {
      type: Date,
    },
    duration: {
      type: Number,
    },
    fee: {
      type: Number,
    },
    car: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: 'Car',
    },
    startedBy: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: 'User',
    },
    rate: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: 'Rate',
    },
  },
);

/**
 * Ends parking session, setting ended time and calculating
 * the parked duration in hours.
 */
sessionSchema.methods.end = function () {
  this.ended = Date();
  this.duration = Math.abs(this.ended - this.started) / 3600000;
  this.calculateFee();
};

/**
   * Calculates the fee and places it in the document.
   *
   * Gets the rate and periods from the rates collection.
   */
sessionSchema.methods.calculateFee = function () {
  const remainder = this.duration % 1;
  let hours = Math.floor(this.duration);
  let fee = 0.0;
  const { periods } = this.rate;

  for (let p = 0; p < periods.length; p += 1) {
    if (periods[p].hours) {
      for (let h = 0; h < periods[h].hours; h += 1) {
        if (hours > 0) {
          fee += periods[p].rate;
          hours -= 1;
        }
      }
    } else {
      while (hours > 0) {
        fee += periods[p].rate;
        hours -= 1;
      }
    }
  }
  fee += periods[periods.length - 1].rate * remainder;
  this.fee = Math.floor(fee);
};

sessionSchema.statics.getOpen = function () {
  return this.find({ $or: [{ ended: null }, { ended: { $exists: false } }] })
    .sort({ started: -1 })
    // .populate('car')
    .populate({
      path: 'car',
      populate: {
        path: 'customer',
        model: 'User',
      },
    })
    .populate('startedBy');
};

sessionSchema.statics.getClosed = function (days = 1) {
  const startPeriod = new Date();
  if (days > 1) {
    startPeriod.setDate(startPeriod.getDate() - days);
  }
  startPeriod.setHours(0, 0, 0, 0);

  // Get today's date at almost midnight
  const endPeriod = new Date();
  endPeriod.setHours(23, 59, 59, 999);

  return this.find({
    ended: {
      $exists: true,
      $gte: startPeriod,
      $lte: endPeriod,
    },
  })
    .sort({ ended: -1 })
    .populate({
      path: 'car',
      populate: {
        path: 'customer',
        model: 'User',
      },
    })
    .populate('startedBy');
};

sessionSchema.add(BaseSchema);

module.exports = mongoose.model('Session', sessionSchema);
