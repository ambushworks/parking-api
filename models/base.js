const mongoose = require('mongoose');

const { Schema } = mongoose;

const baseKeys = {};

const BaseSchema = new Schema(baseKeys, { timestamps: true, discriminatorKey: 'itemtype' });

/* eslint-disable no-param-reassign */
BaseSchema.set('toJSON', {
  transform(doc, ret) {
    ret.id = ret._id.toHexString();
    delete ret._id;
    delete ret.__v;
  },
});
/* eslint-enable no-param-reassign */

module.exports = BaseSchema;
