// eslint-disable-line no-param-reassign
const mongoose = require('mongoose');

const { Schema } = mongoose;

const periodSchema = new Schema({
  hours: {
    type: Number,
  },
  rate: {
    type: Number,
    required: true,
  },
});

const rateSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    default: {
      type: Boolean,
      required: true,
      default: false,
    },
    periods: [periodSchema],
  },
  { timestamps: true },
);

/**
 * Set the current rate's default state to true, and set
 * all others to false.
 */
rateSchema.methods.setDefault = async function () {
  const rates = await this.constructor.find();
  Object.values(rates).forEach((rate) => {
    if (rate._id.toString() === this._id.toString()) {
      if (this.default !== true) {
        this.default = true;
        this.save();
      }
    } else if (rate.default === true) {
      rate.default = false; // eslint-disable-line no-param-reassign
      rate.save();
    }
  });
};

module.exports = mongoose.model('Rate', rateSchema);
