const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');

const config = require('../config/config');

const { Schema } = mongoose;

const BaseSchema = require('./base');

const phoneSchema = new Schema({
  countryCode: {
    type: String,
    required() {
      // Required if number is provided
      return !!this.number;
    },
  },
  number: {
    type: String,
    unique: true,
    required() {
      // Required if countryCode is provided
      return !!this.countryCode;
    },
  },
}, { _id: false });

const userSchema = new Schema({
  email: {
    type: String,
    unique: true,
    required: false,
  },
  phone: {
    type: phoneSchema,
    required: false,
  },
  password: {
    type: String,
    required: false,
    select: false,
  },
  role: {
    type: String,
    enum: ['admin', 'staff', 'user'],
    default: 'user',
  },
});

userSchema.index({ 'phone.countryCode': 1, 'phone.number': 1 }, { unique: true, sparse: true });

/**
 * Validates the jwt and returns the payload.
 *
 * @param {*} req
 * @returns string | jwt.JwtPayload
 */
userSchema.statics.authorize = function (req) {
  const authHeader = req.get('Authorization');
  if (!authHeader) {
    const error = new Error('Not authenticated.');
    error.statusCode = 401;
    throw error;
  }
  const token = authHeader.split(' ')[1];
  let payload;
  try {
    payload = jwt.verify(token, config.acccessTokenSecret);
  } catch (e) {
    if (e instanceof jwt.TokenExpiredError) {
      e.statusCode = 401;
    } else {
      e.statusCode = 500;
    }
    throw e;
  }
  if (!payload) {
    const error = new Error('Not authenticated.');
    error.statusCode = 401;
    throw error;
  }

  return payload;
};

userSchema.add(BaseSchema);

const User = mongoose.model('User', userSchema);

// async function recreateIndexes() {
//   try {
//     await User.syncIndexes();
//     console.log('Indexes synced successfully.');
//   } catch (error) {
//     console.error('Error syncing indexes:', error);
//   }
// }
// // // Call this function when your application starts
// recreateIndexes();

module.exports = User;
