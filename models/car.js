const mongoose = require('mongoose');

const { Schema } = mongoose;

const BaseSchema = require('./base');

const carSchema = new Schema({
  plate: {
    type: String,
    unique: true,
    requred: true,
  },
  lastSession: {
    type: Date,
    required: false,
  },
  customer: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: false,
  },
});

carSchema.statics.searchByPlate = function (plate) {
  const regex = new RegExp(plate, 'i');
  return this.find({ plate: { $regex: regex } });
};

carSchema.statics.withoutOpenSessions = function () {
  return this.aggregate([
    {
      $lookup: {
        from: 'sessions',
        localField: '_id',
        foreignField: 'car',
        as: 'sessions',
      },
    },
    {
      $addFields: {
        id: '$_id',
        ongoingSessions: {
          $filter: {
            input: '$sessions',
            as: 'session',
            cond: { $not: [{ $ifNull: ['$$session.ended', false] }] },
          },
        },
      },
    },
    {
      $lookup: {
        from: 'users', // collection name
        localField: 'customer', // field in the Car document
        foreignField: '_id', // User field that corresponds to 'customer' in Car documents
        as: 'customer', // field in the result array
      },
    },
    {
      $unwind: {
        path: '$customer',
        preserveNullAndEmptyArrays: true, // Preserve cars that might not have a customer
      },
    },
    {
      $match: {
        ongoingSessions: { $size: 0 }, // Match only cars with no ongoing sessions
      },
    },
    {
      $sort: {
        lastSession: -1,
      },
    },
    {
      $limit: 50,
    },
    {
      $project: {
        ongoingSessions: 0, // Optionally remove the ongoingSessions field from the final output
        'customer.password': 0, // Remove passwords from results
      },
    },
  ]).exec();
};

carSchema.add(BaseSchema);

module.exports = mongoose.model('Car', carSchema);
