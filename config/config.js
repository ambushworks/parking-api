require('dotenv').config();

const config = {};

config.serverPort = process.env.NODE_PORT || '8000';

config.acccessTokenSecret = process.env.ACCESS_TOKEN_SECRET || 'DEV_ACCESS';
config.refreshTokenSecret = process.env.REFRESH_TOKEN_SECRET || 'DEV_REFRESH';

// Database configuration
config.deployment = 'development.medflek.mongodb.net';
config.db = 'parking';
config.dbUser = process.env.MONGO_USER;
config.dbPassword = process.env.MONGO_PASSWORD;
config.dbParams = 'retryWrites=true&w=majority';

module.exports = config;
