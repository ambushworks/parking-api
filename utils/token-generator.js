const jwt = require('jsonwebtoken');

const config = require('../config/config');

function TokenGenerator(options) {
  this.accessTokenSecret = config.acccessTokenSecret;
  this.refreshTokenSecret = config.refreshTokenSecret;
  this.options = options;
}

TokenGenerator.prototype.sign = function (payload, signOptions) {
  const jwtSignOptions = { ...signOptions, ...this.options };
  return jwt.sign(payload, this.accessTokenSecret, jwtSignOptions);
};

TokenGenerator.prototype.refresh = function (token, refreshOptions) {
  const payload = jwt.verify(token, this.accessTokenSecret, refreshOptions.verify);
  delete payload.iat;
  delete payload.exp;
  delete payload.nbf;
  delete payload.jti;
  const jwtSignOptions = { ...refreshOptions, ...this.options };
  // The first signing converted all needed options into claims, they are already in the payload
  return jwt.sign(payload, this.refreshTokenSecret, jwtSignOptions);
};

TokenGenerator.prototype.generate = function (userId) {
  const accessToken = this.sign(
    {
      userId,
    },
    { expiresIn: '30m' },
  );
  const refreshToken = this.refresh(
    accessToken,
    { expiresIn: '14d' },
  );
  return { access_token: accessToken, refresh_token: refreshToken };
};

module.exports = TokenGenerator;
